<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="travis_generic">
<packages>
<package name="0603">
<smd name="P$1" x="0.825" y="0" dx="0.8" dy="1" layer="1"/>
<smd name="P$2" x="-0.825" y="0" dx="0.8" dy="1" layer="1"/>
<wire x1="-0.425" y1="0.45" x2="0.425" y2="0.45" width="0.127" layer="21"/>
<wire x1="0.425" y1="-0.45" x2="-0.425" y2="-0.45" width="0.127" layer="21"/>
<text x="0" y="0.9" size="0.7" layer="21" font="vector" align="center">&gt;NAME</text>
<rectangle x1="-0.425" y1="-0.5" x2="0.425" y2="0.5" layer="39"/>
</package>
<package name="HOLES_1">
<pad name="P1" x="0" y="0" drill="0.9"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="-1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="1.016" y1="1.016" x2="-1.016" y2="1.016" width="0.127" layer="21"/>
<wire x1="-1.016" y1="1.016" x2="-1.016" y2="-1.016" width="0.127" layer="21"/>
<text x="0" y="1.524" size="0.7" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RES">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<text x="0" y="2.54" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<wire x1="-2.54" y1="0" x2="-2.286" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.286" y1="1.016" x2="-1.778" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-1.016" x2="-1.27" y2="1.016" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.016" x2="-0.762" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.762" y1="-1.016" x2="-0.254" y2="1.016" width="0.254" layer="94"/>
<wire x1="-0.254" y1="1.016" x2="0.254" y2="-1.016" width="0.254" layer="94"/>
<wire x1="0.254" y1="-1.016" x2="0.762" y2="1.016" width="0.254" layer="94"/>
<wire x1="0.762" y1="1.016" x2="1.27" y2="-1.016" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.016" x2="1.778" y2="1.016" width="0.254" layer="94"/>
<wire x1="1.778" y1="1.016" x2="2.286" y2="-1.016" width="0.254" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.254" layer="94"/>
</symbol>
<symbol name="PAD">
<pin name="P$1" x="-2.54" y="0" visible="off" length="point"/>
<circle x="0" y="0" radius="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.016" y2="0" width="0.1524" layer="94"/>
<text x="5.334" y="0" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES_0603">
<gates>
<gate name="G$1" symbol="RES" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0603">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD">
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="HOLES_1">
<connects>
<connect gate="G$1" pin="P$1" pad="P1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_ixys">
<packages>
<package name="4-SOP">
<smd name="P$1" x="-1.27" y="-2.75" dx="0.55" dy="1.45" layer="1"/>
<smd name="P$2" x="1.27" y="-2.75" dx="0.55" dy="1.45" layer="1"/>
<smd name="P$3" x="1.27" y="2.75" dx="0.55" dy="1.45" layer="1"/>
<smd name="P$4" x="-1.27" y="2.75" dx="0.55" dy="1.45" layer="1"/>
<wire x1="-2.1" y1="-1.95" x2="2.1" y2="-1.95" width="0.127" layer="21"/>
<wire x1="2.1" y1="-1.95" x2="2.1" y2="1.95" width="0.127" layer="21"/>
<wire x1="2.1" y1="1.95" x2="-2.1" y2="1.95" width="0.127" layer="21"/>
<wire x1="-2.1" y1="1.95" x2="-2.1" y2="-1.95" width="0.127" layer="21"/>
<rectangle x1="-2.1" y1="-1.95" x2="2.1" y2="1.95" layer="39"/>
<circle x="-1.4" y="-1.3" radius="0.2" width="0.127" layer="21"/>
<text x="3" y="0" size="1" layer="25" font="vector" rot="R90" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="RELAY_SS_1">
<pin name="I-" x="0" y="0" visible="off" length="short"/>
<pin name="I+" x="0" y="2.54" visible="off" length="short"/>
<pin name="O+" x="10.16" y="2.54" visible="off" length="short" rot="R180"/>
<pin name="O-" x="10.16" y="0" visible="off" length="short" rot="R180"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.302" y1="0.762" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="0.762" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="4.318" y2="1.778" width="0.1524" layer="94"/>
<wire x1="4.318" y1="1.778" x2="3.302" y2="1.778" width="0.1524" layer="94"/>
<wire x1="3.302" y1="1.778" x2="3.81" y2="0.762" width="0.1524" layer="94"/>
<wire x1="4.572" y1="1.524" x2="5.334" y2="1.778" width="0.1524" layer="94"/>
<wire x1="5.334" y1="1.778" x2="5.08" y2="1.524" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.524" x2="5.08" y2="1.778" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.778" x2="5.334" y2="1.778" width="0.1524" layer="94"/>
<wire x1="4.572" y1="0.762" x2="5.334" y2="1.016" width="0.1524" layer="94"/>
<wire x1="5.334" y1="1.016" x2="5.08" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0.762" x2="5.08" y2="1.016" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.016" x2="5.334" y2="1.016" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.794" x2="6.35" y2="2.54" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="2.286" width="0.1524" layer="94"/>
<wire x1="6.35" y1="0.254" x2="6.35" y2="0" width="0.1524" layer="94"/>
<wire x1="6.35" y1="0" x2="6.35" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="5.588" y1="2.286" x2="5.588" y2="0.254" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.778" x2="6.858" y2="1.778" width="0.1524" layer="94"/>
<wire x1="6.858" y1="1.778" x2="6.858" y2="0.762" width="0.1524" layer="94"/>
<wire x1="6.858" y1="0.762" x2="6.35" y2="0.762" width="0.1524" layer="94"/>
<wire x1="5.588" y1="2.286" x2="6.096" y2="2.286" width="0.1524" layer="94"/>
<wire x1="5.588" y1="0.254" x2="6.096" y2="0.254" width="0.1524" layer="94"/>
<wire x1="6.096" y1="1.016" x2="6.096" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="6.096" y1="1.524" x2="6.096" y2="2.794" width="0.1524" layer="94"/>
<wire x1="6.35" y1="2.032" x2="6.35" y2="1.524" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.016" x2="6.35" y2="0.508" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="7.62" y1="2.54" x2="6.35" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.81" x2="7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="7.62" y2="-1.27" width="0.1524" layer="94"/>
<text x="5.08" y="5.08" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="3.81" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="7.62" y2="-1.27" width="0.1524" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CPC1025N">
<gates>
<gate name="G$1" symbol="RELAY_SS_1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4-SOP">
<connects>
<connect gate="G$1" pin="I+" pad="P$1"/>
<connect gate="G$1" pin="I-" pad="P$2"/>
<connect gate="G$1" pin="O+" pad="P$4"/>
<connect gate="G$1" pin="O-" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_smc">
<packages>
<package name="TO-252-3">
<smd name="P$1" x="-2.3" y="-3.3" dx="1.6" dy="3" layer="1"/>
<smd name="P$3" x="2.3" y="-3.3" dx="1.6" dy="3" layer="1"/>
<smd name="P$2" x="0" y="3.35" dx="6.7" dy="6.7" layer="1"/>
<wire x1="-3.3" y1="6.9" x2="3.3" y2="6.9" width="0.127" layer="21"/>
<wire x1="3.3" y1="6.9" x2="3.3" y2="-0.2" width="0.127" layer="21"/>
<wire x1="3.3" y1="-0.2" x2="-3.3" y2="-0.2" width="0.127" layer="21"/>
<wire x1="-3.3" y1="-0.2" x2="-3.3" y2="6.9" width="0.127" layer="21"/>
<rectangle x1="-3.3" y1="-4.9" x2="3.3" y2="6.9" layer="39"/>
<text x="0" y="7.6" size="1" layer="25" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="TRIAC">
<pin name="A1" x="0" y="-2.54" visible="off" length="short" rot="R90"/>
<pin name="A2" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<wire x1="-2.54" y1="-2.54" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="95">&gt;NAME</text>
<pin name="G" x="-2.54" y="-2.54" visible="off" length="point"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SST137K">
<gates>
<gate name="G$1" symbol="TRIAC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="TO-252-3">
<connects>
<connect gate="G$1" pin="A1" pad="P$1"/>
<connect gate="G$1" pin="A2" pad="P$2"/>
<connect gate="G$1" pin="G" pad="P$3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_te">
<packages>
<package name="FSM4JH">
<pad name="P2@1" x="0" y="0" drill="1"/>
<pad name="P2@2" x="6.5" y="0" drill="1"/>
<pad name="P1@1" x="0" y="4.5" drill="1"/>
<pad name="P1@2" x="6.5" y="4.5" drill="1"/>
<wire x1="0.2" y1="-0.8" x2="0.2" y2="5.2" width="0.127" layer="21"/>
<wire x1="0.2" y1="5.2" x2="6.3" y2="5.2" width="0.127" layer="21"/>
<wire x1="6.3" y1="5.2" x2="6.3" y2="-0.8" width="0.127" layer="21"/>
<wire x1="6.3" y1="-0.8" x2="0.2" y2="-0.8" width="0.127" layer="21"/>
<text x="3.2" y="6" size="1" layer="21" font="vector" align="center">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="SWITCH_SPST">
<pin name="P$1" x="-5.08" y="0" visible="off" length="short"/>
<pin name="P$2" x="5.08" y="0" visible="off" length="short" rot="R180"/>
<circle x="-2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.254" layer="94"/>
<circle x="2.54" y="0" radius="0.254" width="0.254" layer="94"/>
<wire x1="1.524" y1="3.048" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="0" y="-1.778" size="1.778" layer="95" font="vector" align="center">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FSM4JH">
<gates>
<gate name="G$1" symbol="SWITCH_SPST" x="0" y="0"/>
</gates>
<devices>
<device name="" package="FSM4JH">
<connects>
<connect gate="G$1" pin="P$1" pad="P1@1 P1@2"/>
<connect gate="G$1" pin="P$2" pad="P2@1 P2@2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="travis_rpi">
<packages>
<package name="PICO_SURFACE">
<wire x1="4" y1="25.5" x2="-4" y2="25.5" width="0.127" layer="21"/>
<wire x1="-4" y1="25.5" x2="-4" y2="26.8" width="0.127" layer="21"/>
<wire x1="-4" y1="26.8" x2="4" y2="26.8" width="0.127" layer="21"/>
<wire x1="4" y1="26.8" x2="4" y2="25.5" width="0.127" layer="21"/>
<wire x1="-4" y1="25.5" x2="4" y2="25.5" width="0.127" layer="22"/>
<wire x1="-4" y1="25.5" x2="-4" y2="26.8" width="0.127" layer="22"/>
<wire x1="-4" y1="26.8" x2="4" y2="26.8" width="0.127" layer="22"/>
<wire x1="4" y1="26.8" x2="4" y2="25.5" width="0.127" layer="22"/>
<wire x1="-10.5" y1="25.5" x2="10.5" y2="25.5" width="0.127" layer="21"/>
<wire x1="10.5" y1="25.5" x2="10.5" y2="-25.5" width="0.127" layer="21"/>
<wire x1="10.5" y1="-25.5" x2="-10.5" y2="-25.5" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-25.5" x2="-10.5" y2="25.5" width="0.127" layer="21"/>
<rectangle x1="-10.5" y1="-25.5" x2="10.5" y2="25.5" layer="39"/>
<rectangle x1="-4" y1="25.5" x2="4" y2="26.8" layer="39"/>
<smd name="VBUS" x="8.89" y="24.13" dx="1.6" dy="1.6" layer="1"/>
<smd name="VSYS" x="8.89" y="21.59" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND0" x="8.89" y="19.05" dx="1.6" dy="1.6" layer="1"/>
<smd name="3V3_EN" x="8.89" y="16.51" dx="1.6" dy="1.6" layer="1"/>
<smd name="3V3(OUT)" x="8.89" y="13.97" dx="1.6" dy="1.6" layer="1"/>
<smd name="ADC_VREF" x="8.89" y="11.43" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP28" x="8.89" y="8.89" dx="1.6" dy="1.6" layer="1"/>
<smd name="AGND" x="8.89" y="6.35" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP27" x="8.89" y="3.81" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP26" x="8.89" y="1.27" dx="1.6" dy="1.6" layer="1"/>
<smd name="RUN" x="8.89" y="-1.27" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP22" x="8.89" y="-3.81" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND6" x="8.89" y="-6.35" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP21" x="8.89" y="-8.89" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP20" x="8.89" y="-11.43" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP19" x="8.89" y="-13.97" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP18" x="8.89" y="-16.51" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND5" x="8.89" y="-19.05" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP17" x="8.89" y="-21.59" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP16" x="8.89" y="-24.13" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP15" x="-8.89" y="-24.13" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP14" x="-8.89" y="-21.59" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND4" x="-8.89" y="-19.05" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP13" x="-8.89" y="-16.51" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP12" x="-8.89" y="-13.97" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP11" x="-8.89" y="-11.43" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP10" x="-8.89" y="-8.89" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND3" x="-8.89" y="-6.35" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP09" x="-8.89" y="-3.81" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP08" x="-8.89" y="-1.27" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP07" x="-8.89" y="1.27" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP06" x="-8.89" y="3.81" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND2" x="-8.89" y="6.35" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP05" x="-8.89" y="8.89" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP04" x="-8.89" y="11.43" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP03" x="-8.89" y="13.97" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP02" x="-8.89" y="16.51" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND1" x="-8.89" y="19.05" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP01" x="-8.89" y="21.59" dx="1.6" dy="1.6" layer="1"/>
<smd name="GP00" x="-8.89" y="24.13" dx="1.6" dy="1.6" layer="1"/>
<smd name="TP3" x="-1" y="24.3" dx="1.6" dy="1.6" layer="1"/>
<smd name="TP2" x="1" y="24.3" dx="1.6" dy="1.6" layer="1"/>
<smd name="TP1" x="0" y="21" dx="1.6" dy="1.6" layer="1"/>
<smd name="TP4" x="-2.5" y="17.5" dx="1.6" dy="1.6" layer="1"/>
<smd name="TP5" x="-2.5" y="15" dx="1.6" dy="1.6" layer="1"/>
<smd name="TP6" x="-2.5" y="12.5" dx="1.6" dy="1.6" layer="1"/>
<smd name="GND7" x="0" y="-23.9" dx="1.6" dy="1.6" layer="1"/>
<smd name="SWDIO" x="2.54" y="-23.9" dx="1.6" dy="1.6" layer="1"/>
<smd name="SWCLK" x="-2.54" y="-23.9" dx="1.6" dy="1.6" layer="1"/>
<circle x="5.7" y="23.5" radius="1.05" width="0.127" layer="21"/>
<circle x="-5.7" y="23.5" radius="1.05" width="0.127" layer="21"/>
<circle x="-5.7" y="-23.5" radius="1.05" width="0.127" layer="21"/>
<circle x="5.7" y="-23.5" radius="1.05" width="0.127" layer="21"/>
<wire x1="-10.5" y1="-10.16" x2="10.5" y2="-10.16" width="0.127" layer="21" style="shortdash"/>
</package>
</packages>
<symbols>
<symbol name="PICO">
<pin name="GP15" x="-17.78" y="-25.4" visible="pin" length="short"/>
<pin name="GP14" x="-17.78" y="-22.86" visible="pin" length="short"/>
<pin name="GND4" x="-17.78" y="-20.32" visible="pin" length="short"/>
<pin name="GP13" x="-17.78" y="-17.78" visible="pin" length="short"/>
<pin name="GP12" x="-17.78" y="-15.24" visible="pin" length="short"/>
<pin name="GP11" x="-17.78" y="-12.7" visible="pin" length="short"/>
<pin name="GP10" x="-17.78" y="-10.16" visible="pin" length="short"/>
<pin name="GND3" x="-17.78" y="-7.62" visible="pin" length="short"/>
<pin name="GP09" x="-17.78" y="-5.08" visible="pin" length="short"/>
<pin name="GP08" x="-17.78" y="-2.54" visible="pin" length="short"/>
<pin name="GP07" x="-17.78" y="0" visible="pin" length="short"/>
<pin name="GP06" x="-17.78" y="2.54" visible="pin" length="short"/>
<pin name="GND2" x="-17.78" y="5.08" visible="pin" length="short"/>
<pin name="GP05" x="-17.78" y="7.62" visible="pin" length="short"/>
<pin name="GP04" x="-17.78" y="10.16" visible="pin" length="short"/>
<pin name="GP03" x="-17.78" y="12.7" visible="pin" length="short"/>
<pin name="GP02" x="-17.78" y="15.24" visible="pin" length="short"/>
<pin name="GND1" x="-17.78" y="17.78" visible="pin" length="short"/>
<pin name="GP01" x="-17.78" y="20.32" visible="pin" length="short"/>
<pin name="GP00" x="-17.78" y="22.86" visible="pin" length="short"/>
<pin name="GP16" x="20.32" y="-25.4" visible="pin" length="short" rot="R180"/>
<pin name="GP17" x="20.32" y="-22.86" visible="pin" length="short" rot="R180"/>
<pin name="GND5" x="20.32" y="-20.32" visible="pin" length="short" rot="R180"/>
<pin name="GP18" x="20.32" y="-17.78" visible="pin" length="short" rot="R180"/>
<pin name="GP19" x="20.32" y="-15.24" visible="pin" length="short" rot="R180"/>
<pin name="GP20" x="20.32" y="-12.7" visible="pin" length="short" rot="R180"/>
<pin name="GP21" x="20.32" y="-10.16" visible="pin" length="short" rot="R180"/>
<pin name="GND6" x="20.32" y="-7.62" visible="pin" length="short" rot="R180"/>
<pin name="GP22" x="20.32" y="-5.08" visible="pin" length="short" rot="R180"/>
<pin name="RUN" x="20.32" y="-2.54" visible="pin" length="short" rot="R180"/>
<pin name="GP26" x="20.32" y="0" visible="pin" length="short" rot="R180"/>
<pin name="GP27" x="20.32" y="2.54" visible="pin" length="short" rot="R180"/>
<pin name="AGND" x="20.32" y="5.08" visible="pin" length="short" rot="R180"/>
<pin name="GP28" x="20.32" y="7.62" visible="pin" length="short" rot="R180"/>
<pin name="ADC_VREF" x="20.32" y="10.16" visible="pin" length="short" rot="R180"/>
<pin name="3.3V(OUT)" x="20.32" y="12.7" visible="pin" length="short" rot="R180"/>
<pin name="3.3V_EN" x="20.32" y="15.24" visible="pin" length="short" rot="R180"/>
<pin name="GND0" x="20.32" y="17.78" visible="pin" length="short" rot="R180"/>
<pin name="VSYS" x="20.32" y="20.32" visible="pin" length="short" rot="R180"/>
<pin name="VBUS" x="20.32" y="22.86" visible="pin" length="short" rot="R180"/>
<wire x1="-15.24" y1="25.4" x2="-15.24" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-27.94" x2="17.78" y2="-27.94" width="0.254" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="17.78" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.78" y1="25.4" x2="-15.24" y2="25.4" width="0.254" layer="94"/>
<text x="0" y="0" size="2.54" layer="95" font="vector" align="center">&gt;NAME</text>
<pin name="SWCLK" x="-2.54" y="-30.48" visible="pin" length="short" rot="R90"/>
<pin name="GND7" x="0" y="-30.48" visible="pin" length="short" rot="R90"/>
<pin name="SWDIO" x="2.54" y="-30.48" visible="pin" length="short" rot="R90"/>
<pin name="TP1" x="-5.08" y="27.94" visible="pin" length="short" rot="R270"/>
<pin name="TP2" x="-2.54" y="27.94" visible="pin" length="short" rot="R270"/>
<pin name="TP3" x="0" y="27.94" visible="pin" length="short" rot="R270"/>
<pin name="TP4" x="2.54" y="27.94" visible="pin" length="short" rot="R270"/>
<pin name="TP5" x="5.08" y="27.94" visible="pin" length="short" rot="R270"/>
<pin name="TP6" x="7.62" y="27.94" visible="pin" length="short" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PICO_SURFACE">
<gates>
<gate name="G$1" symbol="PICO" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PICO_SURFACE">
<connects>
<connect gate="G$1" pin="3.3V(OUT)" pad="3V3(OUT)"/>
<connect gate="G$1" pin="3.3V_EN" pad="3V3_EN"/>
<connect gate="G$1" pin="ADC_VREF" pad="ADC_VREF"/>
<connect gate="G$1" pin="AGND" pad="AGND"/>
<connect gate="G$1" pin="GND0" pad="GND0"/>
<connect gate="G$1" pin="GND1" pad="GND1"/>
<connect gate="G$1" pin="GND2" pad="GND2"/>
<connect gate="G$1" pin="GND3" pad="GND3"/>
<connect gate="G$1" pin="GND4" pad="GND4"/>
<connect gate="G$1" pin="GND5" pad="GND5"/>
<connect gate="G$1" pin="GND6" pad="GND6"/>
<connect gate="G$1" pin="GND7" pad="GND7"/>
<connect gate="G$1" pin="GP00" pad="GP00"/>
<connect gate="G$1" pin="GP01" pad="GP01"/>
<connect gate="G$1" pin="GP02" pad="GP02"/>
<connect gate="G$1" pin="GP03" pad="GP03"/>
<connect gate="G$1" pin="GP04" pad="GP04"/>
<connect gate="G$1" pin="GP05" pad="GP05"/>
<connect gate="G$1" pin="GP06" pad="GP06"/>
<connect gate="G$1" pin="GP07" pad="GP07"/>
<connect gate="G$1" pin="GP08" pad="GP08"/>
<connect gate="G$1" pin="GP09" pad="GP09"/>
<connect gate="G$1" pin="GP10" pad="GP10"/>
<connect gate="G$1" pin="GP11" pad="GP11"/>
<connect gate="G$1" pin="GP12" pad="GP12"/>
<connect gate="G$1" pin="GP13" pad="GP13"/>
<connect gate="G$1" pin="GP14" pad="GP14"/>
<connect gate="G$1" pin="GP15" pad="GP15"/>
<connect gate="G$1" pin="GP16" pad="GP16"/>
<connect gate="G$1" pin="GP17" pad="GP17"/>
<connect gate="G$1" pin="GP18" pad="GP18"/>
<connect gate="G$1" pin="GP19" pad="GP19"/>
<connect gate="G$1" pin="GP20" pad="GP20"/>
<connect gate="G$1" pin="GP21" pad="GP21"/>
<connect gate="G$1" pin="GP22" pad="GP22"/>
<connect gate="G$1" pin="GP26" pad="GP26"/>
<connect gate="G$1" pin="GP27" pad="GP27"/>
<connect gate="G$1" pin="GP28" pad="GP28"/>
<connect gate="G$1" pin="RUN" pad="RUN"/>
<connect gate="G$1" pin="SWCLK" pad="SWCLK"/>
<connect gate="G$1" pin="SWDIO" pad="SWDIO"/>
<connect gate="G$1" pin="TP1" pad="TP1"/>
<connect gate="G$1" pin="TP2" pad="TP2"/>
<connect gate="G$1" pin="TP3" pad="TP3"/>
<connect gate="G$1" pin="TP4" pad="TP4"/>
<connect gate="G$1" pin="TP5" pad="TP5"/>
<connect gate="G$1" pin="TP6" pad="TP6"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
<connect gate="G$1" pin="VSYS" pad="VSYS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="S1" library="travis_ixys" deviceset="CPC1025N" device=""/>
<part name="S2" library="travis_smc" deviceset="SST137K" device=""/>
<part name="R1" library="travis_generic" deviceset="RES_0603" device="" value="100Ohm"/>
<part name="R2" library="travis_generic" deviceset="RES_0603" device="" value="100Ohm"/>
<part name="B1" library="travis_te" deviceset="FSM4JH" device=""/>
<part name="AC1" library="travis_generic" deviceset="PAD" device=""/>
<part name="AC2" library="travis_generic" deviceset="PAD" device=""/>
<part name="5VDC" library="travis_generic" deviceset="PAD" device=""/>
<part name="GND" library="travis_generic" deviceset="PAD" device=""/>
<part name="U$1" library="travis_rpi" deviceset="PICO_SURFACE" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="S1" gate="G$1" x="55.88" y="35.56" smashed="yes">
<attribute name="NAME" x="60.96" y="40.64" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="S2" gate="G$1" x="76.2" y="38.1" smashed="yes">
<attribute name="NAME" x="71.12" y="33.02" size="1.778" layer="95"/>
</instance>
<instance part="R1" gate="G$1" x="50.8" y="38.1" smashed="yes" rot="R180">
<attribute name="NAME" x="50.8" y="40.64" size="1.778" layer="95" font="vector" rot="R180" align="center"/>
</instance>
<instance part="R2" gate="G$1" x="71.12" y="43.18" smashed="yes" rot="R180">
<attribute name="NAME" x="71.12" y="45.72" size="1.778" layer="95" font="vector" rot="R180" align="center"/>
</instance>
<instance part="B1" gate="G$1" x="50.8" y="30.48" smashed="yes">
<attribute name="NAME" x="58.42" y="31.242" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="AC1" gate="G$1" x="81.28" y="43.18" smashed="yes">
<attribute name="NAME" x="86.614" y="43.18" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="AC2" gate="G$1" x="81.28" y="35.56" smashed="yes">
<attribute name="NAME" x="86.614" y="35.56" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="5VDC" gate="G$1" x="81.28" y="27.94" smashed="yes">
<attribute name="NAME" x="86.614" y="27.94" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="GND" gate="G$1" x="81.28" y="25.4" smashed="yes">
<attribute name="NAME" x="86.614" y="25.4" size="1.778" layer="95" font="vector" align="center"/>
</instance>
<instance part="U$1" gate="G$1" x="17.78" y="30.48" smashed="yes">
<attribute name="NAME" x="17.78" y="30.48" size="2.54" layer="95" font="vector" align="center"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="I-"/>
<wire x1="38.1" y1="48.26" x2="40.64" y2="48.26" width="0.1524" layer="91"/>
<wire x1="40.64" y1="48.26" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<wire x1="40.64" y1="35.56" x2="40.64" y2="25.4" width="0.1524" layer="91"/>
<wire x1="40.64" y1="25.4" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<wire x1="55.88" y1="25.4" x2="78.74" y2="25.4" width="0.1524" layer="91"/>
<wire x1="55.88" y1="35.56" x2="40.64" y2="35.56" width="0.1524" layer="91"/>
<junction x="40.64" y="35.56"/>
<pinref part="B1" gate="G$1" pin="P$2"/>
<wire x1="55.88" y1="30.48" x2="55.88" y2="25.4" width="0.1524" layer="91"/>
<junction x="55.88" y="25.4"/>
<pinref part="GND" gate="G$1" pin="P$1"/>
<pinref part="U$1" gate="G$1" pin="GND0"/>
</segment>
</net>
<net name="PA7" class="0">
<segment>
<wire x1="38.1" y1="38.1" x2="45.72" y2="38.1" width="0.1524" layer="91"/>
<pinref part="R1" gate="G$1" pin="P$2"/>
<pinref part="U$1" gate="G$1" pin="GP28"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="R1" gate="G$1" pin="P$1"/>
<pinref part="S1" gate="G$1" pin="I+"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="G"/>
<pinref part="S1" gate="G$1" pin="O-"/>
<wire x1="73.66" y1="35.56" x2="66.04" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="S1" gate="G$1" pin="O+"/>
<pinref part="R2" gate="G$1" pin="P$2"/>
<wire x1="66.04" y1="43.18" x2="66.04" y2="38.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="P$1"/>
<pinref part="S2" gate="G$1" pin="A2"/>
<pinref part="AC1" gate="G$1" pin="P$1"/>
<wire x1="76.2" y1="43.18" x2="78.74" y2="43.18" width="0.1524" layer="91"/>
<junction x="76.2" y="43.18"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="S2" gate="G$1" pin="A1"/>
<wire x1="76.2" y1="35.56" x2="78.74" y2="35.56" width="0.1524" layer="91"/>
<pinref part="AC2" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="38.1" y1="33.02" x2="45.72" y2="33.02" width="0.1524" layer="91"/>
<wire x1="45.72" y1="33.02" x2="45.72" y2="30.48" width="0.1524" layer="91"/>
<pinref part="B1" gate="G$1" pin="P$1"/>
<pinref part="U$1" gate="G$1" pin="GP27"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="38.1" y1="50.8" x2="43.18" y2="50.8" width="0.1524" layer="91"/>
<wire x1="43.18" y1="50.8" x2="43.18" y2="27.94" width="0.1524" layer="91"/>
<wire x1="43.18" y1="27.94" x2="78.74" y2="27.94" width="0.1524" layer="91"/>
<pinref part="5VDC" gate="G$1" pin="P$1"/>
<pinref part="U$1" gate="G$1" pin="VSYS"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
