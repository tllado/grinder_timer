/**
 * Copyright (c) 2022 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "pico/stdlib.h"

#define PIN_TRIAC     28U
#define PIN_BUTTON    27U
#define DELAY_LENGTH  200   // ms
#define TICK_PERIOD   50    // ms
#define RUN_LENGTH    6000  // ms

int main() {
  // Initialize Button
  gpio_init(PIN_BUTTON);
  gpio_set_dir(PIN_BUTTON, GPIO_IN);
  gpio_pull_up(PIN_BUTTON);

  // Initialize Onboard LED
  gpio_init(PICO_DEFAULT_LED_PIN);
  gpio_set_dir(PICO_DEFAULT_LED_PIN, GPIO_OUT);
  gpio_put(PICO_DEFAULT_LED_PIN, false);

  // Initialize TRIAC
  gpio_init(PIN_TRIAC);
  gpio_set_dir(PIN_TRIAC, GPIO_OUT);
  gpio_put(PIN_TRIAC, false);

  uint32_t counter{0U};

  while (true) {
    const bool button_pressed = !gpio_get(PIN_BUTTON);

    if(button_pressed) {
      counter += TICK_PERIOD;
    }
    else {
      counter = 0U;
    }

    if(counter >= DELAY_LENGTH) {
      gpio_put(PICO_DEFAULT_LED_PIN, true);
      gpio_put(PIN_TRIAC, true);

      sleep_ms(RUN_LENGTH);

      gpio_put(PICO_DEFAULT_LED_PIN, false);
      gpio_put(PIN_TRIAC, false);
      counter = 0U;
    }
    else {
      sleep_ms(TICK_PERIOD);
    }
  }
}
