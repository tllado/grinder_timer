# grinder_timer

I made a timer that runs my Baratza Virtuoso coffee grinder for a predetermined length of time. Essentially turns the Virtuoso into a Virtuoso+.

Uses a Raspberry Pi Pico and an expansion board containing a button and a TRIAC.